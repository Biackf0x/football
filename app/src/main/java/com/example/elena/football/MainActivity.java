package com.example.elena.football;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Integer counter = 0;
    private Integer counter2 = 0;
    final String LOG_TAG = "myLogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickBtnAdd1(View view) {
        if (counter <= 98) {
            counter++;
            TextView counterView = (TextView) findViewById(R.id.txt_counter);
            counterView.setText(counter.toString());
        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Огоо! Мы уже подали заявку в книгу рекордов Гиннеса", Toast.LENGTH_SHORT);
            toast.show();
            counter=0;
            TextView counterView = (TextView) findViewById(R.id.txt_counter);
            counterView.setText(counter.toString());
        }
    }

    public void onClickBtnAdd2(View view) {
        if (counter2 <= 98) {
            counter2++;
            TextView counterView = (TextView) findViewById(R.id.txt_counter2);
            counterView.setText(counter2.toString());
        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Огоо! Мы уже подали заявку в книгу рекордов Гиннеса", Toast.LENGTH_SHORT);
            toast.show();
            counter2=0;
            TextView counterView2 = (TextView) findViewById(R.id.txt_counter2);
            counterView2.setText(counter2.toString());
        }
    }

    public void onClickBtnAdd3(View view) {
            counter=0;
            counter2=0;
            TextView counterView = (TextView) findViewById(R.id.txt_counter);
            counterView.setText(counter.toString());
        TextView counterView2 = (TextView) findViewById(R.id.txt_counter2);
        counterView2.setText(counter2.toString());

    }


    //Фиксим обнуление методом сохранения
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count", counter);
        outState.putInt("count2", counter2);
        Log.d(LOG_TAG, "onSaveInstanceState");
    }

    //Метод восстановления. Из savedInstanceState вытаскиваем значение и помещаем в переменную count.
// Теперь при уничтожении и воссоздании Activity переменная count сохранит свое значение, и наш счетчик продолжит работать
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        counter = savedInstanceState.getInt("count");
        counter2 = savedInstanceState.getInt("count2");
        Log.d(LOG_TAG, "onRestoreInstanceState");
    }
}